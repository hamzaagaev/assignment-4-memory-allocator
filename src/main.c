#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

#ifndef MAP_ANONYMOUS
#define MAP_ANONYMOUS 0x20
#endif

static const char* line = "-----------------------------------------------------------\n";
void PRNL() {
    printf("\n");
}

struct block_header* get_block_header_ptr(void* block_container) {
    struct block_header* result_ptr = block_container - offsetof(struct block_header, contents);
    return result_ptr;
}

void test_malloc() {
    printf("%s", line);
    printf("Test 1: Malloc test.\n");
    PRNL();
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(10);
    assert(block1 != NULL);


    debug_heap(stdout, heap);
    PRNL();

    _free(block1);
    heap_term();
    printf("\n");
}

void test_free_one() {
    printf("%s", line);
    printf("Test 2: Free one block test.\n");
    PRNL();
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(128);
    void* block2 = _malloc(300);
    void* block3 = _malloc(500);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);

    debug_heap(stdout, heap);
    PRNL();

    _free(block3);
    struct block_header* block3_head = get_block_header_ptr(block3);
    assert(block3_head->is_free);
    printf("3rd block freed. Now it is looks like this:\n");
    debug_struct_info(stdout, block3_head);
    PRNL();

    debug_heap(stdout, heap);
    PRNL();

    heap_term();
    printf("\n");
}

void test_free_two() {
    printf("%s", line);
    printf("Test 3: Free two blocks test.\n");
    PRNL();
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(128);
    void* block2 = _malloc(203);
    void* block3 = _malloc(305);
    void* block4 = _malloc(779);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    assert(block4 != NULL);

    debug_heap(stdout, heap);
    PRNL();

    _free(block2);
    _free(block4);
    struct block_header* block2_head = get_block_header_ptr(block2);
    struct block_header* block4_head = get_block_header_ptr(block4);
    assert(block2_head->is_free);
    assert(block4_head->is_free);
    printf("2nd block freed. Now it is looks like this:\n");
    debug_struct_info(stdout, block2_head);
    PRNL();
    printf("4th block freed. Now it is looks like this:\n");
    debug_struct_info(stdout, block4_head);
    PRNL();

    debug_heap(stdout, heap);
    PRNL();

    heap_term();
    printf("\n");
}

void test_grow_heap() {
    printf("%s", line);
    printf("Test 4: Grow heap test.\n");
    PRNL();
    void* heap = heap_init(0);
    assert(heap != NULL);

    printf("Clear heap created.\n");
    debug_heap(stdout, heap);
    PRNL();

    void* block1 = _malloc(REGION_MIN_SIZE);
    void* block2 = _malloc(REGION_MIN_SIZE / 2);
    assert(block1 != NULL);
    assert(block2 != NULL);

    struct block_header* block1_head = get_block_header_ptr(block1);
    struct block_header* block2_head = get_block_header_ptr(block2);

    printf("Huge blocks added to heap:\n");
    printf("1st block: ");
    debug_struct_info(stdout, block1_head);
    printf("2nd block: ");
    debug_struct_info(stdout, block2_head);
    PRNL();

    debug_heap(stdout, heap);
    PRNL();

    heap_term();
    printf("\n");
}

void test_next_page_is_taken() {
    printf("%s", line);
    printf("Test 5: Next page is taken test.\n");
    PRNL();

    void* heap = heap_init(0);
    assert(heap != NULL);

    printf("Clear heap created.\n");
    debug_heap(stdout, heap);
    PRNL();

    size_t taken_region_size = 3 * REGION_MIN_SIZE;

    void* taken_region = mmap(heap + REGION_MIN_SIZE, taken_region_size, 
        PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(taken_region != NULL);

    printf("mmap at address: %p\n", heap + REGION_MIN_SIZE);
    PRNL();

    void* block = _malloc(2 * REGION_MIN_SIZE);
    assert(block != NULL);

    struct block_header* block_head = get_block_header_ptr(block);

    printf("New huge block alllocated: ");
    debug_struct_info(stdout, block_head);
    PRNL();

    debug_heap(stdout, heap);
    PRNL();

    munmap(taken_region, taken_region_size);

    heap_term();
    printf("\n");
}

int main() {
    test_malloc();
    test_free_one();
    test_free_two();
    test_grow_heap();
    test_next_page_is_taken();
    return 0;
}