#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    // вычисление размера региона
    size_t querying_size = (size_from_capacity((block_capacity) {.bytes=query})).bytes;
    size_t actual_size = region_actual_size(querying_size);

    // запрос региона с заданным размером и начальным адресом
    void* alloc_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE); // MAP_FIXED_NOREPLACE

    // если не получилось выделить регион в нужном адресе
    if (alloc_addr == MAP_FAILED) {
        alloc_addr = map_pages(addr, actual_size, 0);
        if (alloc_addr == MAP_FAILED) return REGION_INVALID;
    }

    // если получилось выделить регион в запрашиваемом адресе, то он идет сразу после другого региона
    bool extends = (alloc_addr == addr);

    block_init(alloc_addr, (block_size) {.bytes = actual_size}, NULL);
    return (struct region) {.addr = alloc_addr, .size = actual_size, .extends = extends};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    struct block_header* begin_block = HEAP_START;
    struct block_header* curr_block = begin_block;
    size_t curr_size = 0;
    //
    while (curr_block) {
        struct block_header* next_block = curr_block->next;
        curr_size += size_from_capacity(curr_block->capacity).bytes;
        if (block_after(curr_block) != next_block) {
            int is_munmapped = munmap(begin_block, curr_size);
            if (is_munmapped == -1) {
				fprintf(stderr, "Cannot munmap region with params: addr - %p; len - %lu.\n", (void*) begin_block, curr_size);
			}
			curr_size = 0;
            begin_block = next_block;
        } else if (!next_block) {
            int is_munmapped = munmap(begin_block, curr_size);
			if (is_munmapped == -1) {
				fprintf(stderr, "Cannot munmap region with params: addr - %p; len - %lu.\n", (void*) begin_block, curr_size);
			}
            break;
        }
        curr_block = next_block;
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    // проверка на то что блок можно разделить, если можно то делим (создаем новый блок сразу после старого)
    if (block && block_splittable(block, query)) {
        void* second_block = block->contents + query;
        block_init(second_block, (block_size) {.bytes = block->capacity.bytes - query}, block->next);
        block->next = second_block;
        block->capacity = (block_capacity) {.bytes = query};
        return true;
    }
    return false;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    // если блок можно слить со следующим, объединяем и возвращаем 1, иначе 0
    struct block_header* next_block = block->next;
    if (next_block && mergeable(block, next_block)) {
        block->next = next_block->next;
        // неважно, какой заголовок останется у второго блока, ничего с ним не делаем
        block->capacity = (block_capacity) {.bytes = block->capacity.bytes
                                            + size_from_capacity(next_block->capacity).bytes};
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header* curr_block = block;
    struct block_search_result result = (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
    // проходимся по списку, объединяя блоки и ищем хороший
    while (curr_block) {
        while (try_merge_with_next(curr_block));
        if (curr_block->is_free && block_is_big_enough(sz, curr_block)) {
            result.type = BSR_FOUND_GOOD_BLOCK;
            result.block = curr_block;
            break;
        }
        if (!curr_block->next) {
            result.type = BSR_REACHED_END_NOT_FOUND;
            // возвращает последний блок!
            result.block = curr_block;
            break;
        }
        curr_block = curr_block->next;
    }
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    // ищем свободный хороший блок, если находим, пытаемся поделить, делаем занятым
    struct block_search_result existing_block = find_good_or_last(block, query);
    if (existing_block.type != BSR_FOUND_GOOD_BLOCK)
        return existing_block;
    split_if_too_big(existing_block.block, query);
    existing_block.block->is_free = false;
    return existing_block;
}


// пробуем увеличить кучу
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    // ищем адрес, следующий за последним блоком и пытаемся создать в нем регион
    void* next_addr = block_after(last);
    struct region next_region = alloc_region(next_addr, query);
    if (region_is_invalid(&next_region)) {
        return NULL;
    }
    // меняем указатель у последнего блока и пытаемся объединить с регионом
    // если получается, возвращаем последний блок, иначе регион
    last->next = next_region.addr;
    bool is_continuous = try_merge_with_next(last);
    if (is_continuous) return last;
    else return next_region.addr;   // если нельзя продолжить блок, неважно, сама ли ОС его выделила или нет
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    if (!heap_start) {
        struct region new_region = alloc_region(NULL, query);
        if (region_is_invalid(&new_region)) return NULL;
        heap_start = new_region.addr;
    }
    struct block_search_result block_search = try_memalloc_existing(query, heap_start);
    if (block_search.type == BSR_CORRUPTED) return NULL;
    struct block_header* new_block;
    if (block_search.type != BSR_FOUND_GOOD_BLOCK) {
        new_block = grow_heap(block_search.block, query);
        new_block = try_memalloc_existing(query, new_block).block;
    } else {
        new_block = block_search.block;
    }
    return new_block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));      // объединяем все свободные блоки, начиная с заданного
}
